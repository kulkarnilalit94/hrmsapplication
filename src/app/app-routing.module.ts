import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import {RegisterComponent} from './register/register.component';
import { AddDetailsComponent } from './add-details/add-details.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';


const routes: Routes = [
  {path: '', component: LoginComponent},
  { path:"Login" , component : LoginComponent},
  {path:"Register",component :RegisterComponent},
  {path:"AddUser",component :AddDetailsComponent},
  {path:"update-password",component :UpdatePasswordComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LoginComponent,RegisterComponent,AddDetailsComponent]
