import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(public fb: FormBuilder) { }


  ngOnInit() {
  }


  Location: any = ["Bengaluru", "Pune"]


  /*########### Form ###########*/
  registerForm = this.fb.group({
    locationName: ['', [Validators.required]]
  })



  changeLocation(e) {
    console.log(e.value)
    this.locationName.setValue(e.target.value, {
      onlySelf: true
    })
  }

  get locationName() {
    return this.registerForm.get("locationName");
  }
}
